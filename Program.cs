﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beginner.programs
{
    public delegate int Add(int x, int y);
    class Program
    {
        static void Main(string[] args)
        {
            //WorkHandler del1 = new WorkHandler(workPerformed1);
            //WorkHandler del2 = new WorkHandler(workPerformed2);
            //WorkHandler del3 = new WorkHandler(workPerformed3);

            //del1 += del2 + del3;
            //PerformWork(del1);
            Add addDel = (x, y) => x + y;
            Action<int, int> myAction = (x, y) => Console.WriteLine(x + y);

            var pd = new ProcessData();
            pd.process(10, 58, addDel);
            pd.ProcessAction(25, 54, myAction);
            Worker worker = new Worker();
            worker.WorkPerformed += (s, e) =>
            {
                Console.WriteLine("hours worked: " + e.Hours + " " + "type of work: " + e.WorkType);
            };
            worker.WorkCompleted += (s, e) =>
            {
                Console.WriteLine("worker is done");
            };
            worker.DoWork(8, WorkType.Code);

            Code c = new Code();
            c.CodeCompleted += (s, e) =>
              {
                  Console.WriteLine("hours of : " + e.Hours + " " + "type of event: " + e.WorkType);
              };

            c.WatchVideos += (s, e) =>
              {
                  Console.WriteLine("job is done... coder is resting");
              };
            c.doJob(5, Xcdify.code);
            Console.Read();

        }

        //static void worker_workPerformed(object sender,EventPerformedArgs e)
        //{
        //    Console.WriteLine("hours worked: "+e.Hours + " " +"type of work: "+ e.WorkType);
        //}

        //static void worker_workCompleted(object sender,EventArgs e)
        //{
        //    Console.WriteLine("worker is done");
        //}

        //static void PerformWork(WorkHandler del)
        //{
        //    del(5, WorkType.Code);
        //    del(10, WorkType.GotoMeetings);
        //}

        //static Int32 workPerformed1(int hours,WorkType workType)
        //{
        //    Console.WriteLine("work_1 is performed "+hours +" "+ workType);
        //    return hours;
        //}

        //static Int32 workPerformed2(int hours, WorkType workType)
        //{
        //    Console.WriteLine("work_2 is performed " + hours + " " + workType);
        //    return hours;
        //}

        //static Int32 workPerformed3(int hours, WorkType workType)
        //{
        //    Console.WriteLine("work_3 is performed " + hours + " " + workType);
        //    return hours;
        //}




    }

    public enum WorkType
    {
        GotoMeetings,
        Code,
        GenerateReports
    }

    public enum Xcdify
    {
        code,
        WatchPluralsight
    }
    
}



    