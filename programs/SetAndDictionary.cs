﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beginner.programs
{
    class SetAndDictionary
    {
        public void setDemo()
        {

            Dictionary<int, string> dict = new Dictionary<int, string>();
            dict.Add(1, "george washington");
            dict.Add(2, "john f kennedy");
            foreach (KeyValuePair<int, string> data in dict)
            {
                Console.WriteLine(data);

            }

            HashSet<string> hset = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
            hset.Add("bangalore");
            hset.Add("mysore");
            hset.Add("BANGALORE");
            foreach (string item in hset)
            {
                Console.WriteLine(item);
            }

            HashSet<string> hset2 = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
            hset2.Add("bangalore");
            hset2.Add("mysore");
            hset2.Add("delhi");
            hset2.Union(hset);
        }
    }
}
