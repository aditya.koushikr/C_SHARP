﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beginner.programs
{
    class ListAndLinkedList
    {
        public void linkedDemo()
        {
            List<string> presidents = new List<string>();
            presidents.Add("bill clinton");
            presidents.Add("george bush");
            presidents.Add("barack Obama");



            foreach (string president in presidents)
            {
                Console.WriteLine("the presidents are: " + president);
            }
            Console.WriteLine(presidents.Count);
            Console.WriteLine(presidents.Capacity);

            presidents.Add("George Washington");
            presidents.Add("John F Kennady");
            presidents.Add("Abraham Lincoln");

            foreach (string president in presidents)
            {
                Console.WriteLine("the presidents are: " + president);
            }

            Console.WriteLine(presidents.Count);
            Console.WriteLine(presidents.Capacity);

            Console.WriteLine(presidents[0] + " " + presidents[1]);
            Console.WriteLine(presidents.GetType());
            Console.WriteLine(presidents.GetHashCode());


            NonBlankString ns = new NonBlankString();
            ns.Add("aditya");
            ns.Insert(1, "koushik");
            ns.Add(" ");
            foreach (string data in ns)
            {
                Console.WriteLine(data);
            }

            LinkedList<string> llist=new LinkedList<string>();
            llist.AddFirst("john kennady");
            llist.AddLast("barack obama");
            foreach(string data in llist)
            {
                Console.WriteLine(data);
            }

            Console.Read();



        }




    }
}

class NonBlankString : Collection<string>
{
    protected override void InsertItem(int index, string item)
    {
        if (string.IsNullOrWhiteSpace(item))
        {
            throw new ArgumentNullException("items cannot be null");
        }

        base.InsertItem(index, item);
    }

    protected override void SetItem(int index, string item)
    {
        if (string.IsNullOrWhiteSpace(item))
        {
            throw new ArgumentNullException("cannot set null or white space values..");

        }
        base.SetItem(index, item);
    }

    internal void Add(string v)
    {
        throw new NotImplementedException();
    }
}
        