﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beginner.programs
{
    class Code
    {
        public event EventHandler<CodeEvent> CodeCompleted;
        public event EventHandler WatchVideos;

        public void doJob(int hours,Xcdify workType)
        {
            for(int i=0;i<hours;i++)
            {
                System.Threading.Thread.Sleep(680);
                PerformCoding(i+1,workType);
            }
            WatchPluralsightVideos();
        }

        protected virtual void PerformCoding(int hours,Xcdify workType)
        {
            var del = CodeCompleted as EventHandler<CodeEvent>;
            if(del!=null)
            {
                del(this, new CodeEvent(hours, workType));
            }
        }

        protected virtual void WatchPluralsightVideos()
        {
            var del = WatchVideos as EventHandler;
            if(del!=null)
            {
                del(this, CodeEvent.Empty);
            }
        }
    }
}
