﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beginner.programs
{
    public class ProcessData
    {
        public void process(int x,int y,Add del)
        {
            var result = del(x, y);
            Console.WriteLine(result);
        }

        public void ProcessAction(int x,int y,Action<int,int> action)
        {
            action(x, y);
            Console.WriteLine("Action has been processed");
        }
    }
}
