﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beginner.programs
{
    class Arrays
    {
        public static void demo(String[] args)
        {
            string[] days = { "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday" };
            Console.WriteLine("enter the index: ");

            int id = int.Parse(Console.ReadLine());
            Console.WriteLine(days[id]);
            foreach (string day in days)
            {
                Console.WriteLine(day);
            }


            Console.WriteLine(days.IsFixedSize);
            Console.WriteLine(days.IsSynchronized);
            Console.WriteLine(days.IsReadOnly);

            for (int i = 0; i < days.Length; i++)
            {
                Console.WriteLine(days[i]);
            }


            var days2 = days;
            Console.WriteLine("checking the references.. " + ReferenceEquals(days, days2));
            Console.WriteLine(days == days2);
            Array.Reverse(days);
            foreach (string day in days)
            {
                Console.WriteLine(day);
            }

            var comparer = new StringCompare();
            int ind = Array.IndexOf(days, "tuesday");
            string[] all = Array.FindAll(days, x => x.Length == 6);
            Console.WriteLine(ind);
            foreach (string item in all)
            {
                Console.WriteLine(item);
            }
            Console.Read();



        }
    }


    class StringCompare : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            return x.Length.CompareTo(y.Length);
        }
    }
}
    

