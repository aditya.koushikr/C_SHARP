﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beginner.programs
{
    class CodeEvent:EventArgs
    {
        public CodeEvent(int hours,Xcdify workType)
        {
            Hours = hours;
            WorkType = workType;
        }

        public int Hours { get; set; }
        public Xcdify WorkType { get; set; }
    }
}
