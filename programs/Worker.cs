﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beginner.programs
{
    
    public class Worker
    {
        public event EventHandler<EventPerformedArgs> WorkPerformed;
        public event EventHandler WorkCompleted;
        public void DoWork(int hours,WorkType workType)
        {
            for(int i=0;i<hours;i++)
            {
                System.Threading.Thread.Sleep(500);
                OnPerformed(i+1, workType);
            }
            OnCompleted();
        }

        protected virtual void OnPerformed(int hours,WorkType workType)
        {
            var del = WorkPerformed as EventHandler<EventPerformedArgs>;
            if (del != null)
            {
                del(this,new EventPerformedArgs(hours,workType));
            }
        }

        protected virtual void OnCompleted()
        {
            var del = WorkCompleted as EventHandler;
            if (del != null)
            {
                del(this, EventPerformedArgs.Empty);
            }
        }
    }
}
