﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beginner.programs
{
    class Class4
    {
        String _name;
        

        public String Name
        {
            get
            {
                return _name.ToUpper();
            }
            set
            {
                if(!String.IsNullOrEmpty(value))
                {
                    _name = value;
                    Console.WriteLine(_name);
                    Console.Read();
                }
            }
        }
    }
}
